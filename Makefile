ELM_BUILD := elm make src/Main.elm --output=dist/annotator.js


dist: clean prep
	$(ELM_BUILD) --optimize


debug: clean prep
	$(ELM_BUILD) --debug


watch:
	@while inotifywait -e close_write $(shell find src -name *.elm) index.js; do echo; $(MAKE) debug; echo; done


watch-dist:
	@while inotifywait -e close_write $(shell find src -name *.elm) index.js; do echo; $(MAKE) dist; echo; done


prep:
	@mkdir -p dist
	@cp index.js dist/


clean:
	@rm -rf dist/*
