ID Recognition Annotator
========================

A simple webapp to friendsource ground truths.


Setup
-----

You need [Elm](elm-lang.org/), [PostgREST](postgrest.org/), [jwt-cli](https://github.com/mike-engel/jwt-cli), and [Pipenv](https://pipenv.kennethreitz.org/en/latest/).

Set up the python environment:
```
pipenv install
```

Then define a secret for signing JWTs:
```
cat /dev/urandom | tr -dc A-Za-z0-9 | head -c32 > postgrest/jwt-secret
```

And define a password for the `authenticator` role on the database:
```
sed -i "s/CHANGETHIS/"(cat /dev/urandom | tr -dc A-Za-z0-9 | head -c32)"/g" postgrest/{conf,schema.sql}
```

Usage
-----

Reset the postgresql database and serve it:
```
sudo -u postgres dropdb id_recognition_dataset
sudo -u postgres createdb id_recognition_dataset
sudo -u postgres psql -d id_recognition_dataset < postgrest/schema.sql
postgrest postgrest/conf
```

Generate a token for user `my-user` lasting 5 days:
```
tools/jwt.sh postgrest/jwt-secret my-user "5 days"
```

Load a dataset into the postgresql database:
```
pipenv run tools/import.py <base-url> <path-to-dataset-folder> <user-jwt>
```

Export a dataset back to local files:
```
pipenv run tools/export.py <base-url> <path-to-dataset-folder> <overwrite> <user-jwt>
```

Build the Elm app: `make` (`make watch` to build continuously)

Serve the Elm app with live reloading: `npm run serve`
