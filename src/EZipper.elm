module EZipper exposing (EZipper, empty, filterAndReset, fromListSelectHead, fromParts, isEmpty, length, map, selectWhen, selection, toList)

import List.Extra
import Zipper exposing (Zipper(..))


type EZipper a
    = EZipper (Maybe (Zipper a))


isEmpty : EZipper a -> Bool
isEmpty (EZipper maybeZipper) =
    maybeZipper == Nothing


empty : EZipper a
empty =
    EZipper Nothing


length : EZipper a -> Int
length =
    toList >> List.length


fromListSelectHead : List a -> EZipper a
fromListSelectHead list =
    case list of
        [] ->
            empty

        head :: tail ->
            fromParts [] head tail


fromParts : List a -> a -> List a -> EZipper a
fromParts head sel tail =
    EZipper <| Just <| Zipper head sel tail


toList : EZipper a -> List a
toList (EZipper maybeZipper) =
    case maybeZipper of
        Nothing ->
            []

        Just zipper ->
            Zipper.toList zipper


selectWhen : (a -> Bool) -> EZipper a -> EZipper a
selectWhen func ezipper =
    case List.Extra.splitWhen func (toList ezipper) of
        Just ( head, sel :: tail ) ->
            fromParts head sel tail

        _ ->
            ezipper


selection : EZipper a -> Maybe a
selection (EZipper maybeZipper) =
    Maybe.map Zipper.extract maybeZipper


map : (a -> b) -> EZipper a -> EZipper b
map func (EZipper maybeZipper) =
    Maybe.map (Zipper.map func) maybeZipper
        |> EZipper


filterAndReset : (a -> Bool) -> EZipper a -> EZipper a
filterAndReset func ezipper =
    case List.filter func (toList ezipper) of
        [] ->
            empty

        head :: tail ->
            fromParts [] head tail
