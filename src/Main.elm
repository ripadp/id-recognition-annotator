module Main exposing (Msg(..), main, update, view)

import Base64
import Browser
import Bytes
import Bytes.Decode as BD
import EZipper exposing (EZipper(..))
import Element as El
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html, button, div, form, img, input, li, p, text, ul)
import Html.Attributes exposing (disabled, placeholder, src, value)
import Html.Events as HE
import Http
import Json.Decode as D
import Json.Encode as E
import Jwt exposing (decodeToken)
import List.Extra as LE
import Maybe
import Maybe.Extra as ME
import Parser exposing ((|.), (|=))
import Postgrest.Client as P
import Selector
    exposing
        ( Annotation
        , Image
        , ImageAnnotation
        , Point
        , Points
        , Selection(..)
        , Selector
        , isAnnotatedSelected
        , isBadSelected
        , isEditSelected
        , lengthAnnotated
        , lengthBad
        , makeSelectorAtEdit
        , selectAnnotated
        , selectBad
        , selectEdit
        , selectedAnnotation
        , selectedImage
        , selectorAnnotated
        , selectorBad
        , selectorEdit
        , selectorSelection
        , setEdit
        , updateImage
        )
import String
import Svg
import Svg.Attributes as SA
import Svg.Events as SE
import Task
import Time
import Zipper exposing (Zipper(..))


main =
    Browser.element
        { init = \windowSize -> ( initModel windowSize, Cmd.none )
        , update = update
        , view = El.layout [] << view
        , subscriptions = \_ -> Sub.none
        }



-- API


backendBaseurl : String
backendBaseurl =
    "//localhost:3000"


primaryKey : P.PrimaryKey Int
primaryKey =
    P.primaryKey ( "id", P.int )


imagesCountEndpoint : P.Endpoint ( Int, Int )
imagesCountEndpoint =
    P.endpoint (backendBaseurl ++ "/images_count") decodeImagesCount


annotationsCountEndpoint : P.Endpoint Int
annotationsCountEndpoint =
    P.endpoint (backendBaseurl ++ "/annotations_count") decodeCount


imageAnnotationsEndpoint : P.Endpoint ImageAnnotation
imageAnnotationsEndpoint =
    P.endpoint (backendBaseurl ++ "/images_with_annotations") decodeImageAnnotation


imageNullAnnotationsEndpoint : P.Endpoint Image
imageNullAnnotationsEndpoint =
    P.endpoint (backendBaseurl ++ "/images_with_annotations") decodeImage


imagesEndpoint : P.Endpoint Image
imagesEndpoint =
    P.endpoint (backendBaseurl ++ "/images") decodeImage


annotationsEndpoint : P.Endpoint Annotation
annotationsEndpoint =
    P.endpoint (backendBaseurl ++ "/annotations") decodeAnnotation


imageNoBase64Attrs : List String
imageNoBase64Attrs =
    [ "id"
    , "path"
    , "bad_for"
    , "last_lock"
    ]


imageAnnotationsNoBase64Attrs : List String
imageAnnotationsNoBase64Attrs =
    imageNoBase64Attrs
        ++ [ "annotation_annotator"
           , "annotation_created_at"
           , "annotation_id"
           , "annotation_points"
           ]


userImagesBadTask : P.JWT -> String -> Task.Task P.Error (List Image)
userImagesBadTask jwt user =
    P.getMany imageNullAnnotationsEndpoint
        |> P.setParams
            [ P.param "bad_for" <| P.eq <| P.string user
            , P.select <| P.attributes imageAnnotationsNoBase64Attrs
            ]
        |> P.toTask jwt


userImageAnnotationsTask : P.JWT -> String -> Task.Task P.Error (List ImageAnnotation)
userImageAnnotationsTask jwt user =
    P.getMany imageAnnotationsEndpoint
        |> P.setParams
            [ P.param "annotation_annotator" <| P.eq <| P.string user
            , P.param "bad_for" P.null
            , P.order [ P.desc "annotation_created_at" ]
            , P.select <| P.attributes imageAnnotationsNoBase64Attrs
            ]
        |> P.toTask jwt


userImagesBadAndAnnotatedImagesTask : P.JWT -> String -> Task.Task P.Error ( List Image, List ImageAnnotation )
userImagesBadAndAnnotatedImagesTask jwt user =
    Task.map2 Tuple.pair (userImagesBadTask jwt user) (userImageAnnotationsTask jwt user)


getImage : Int -> P.Request Image
getImage =
    P.getByPrimaryKey imagesEndpoint primaryKey


unannotatedImageTask : P.JWT -> Task.Task P.Error (Maybe Image)
unannotatedImageTask jwt =
    let
        getTask =
            P.getMany imageNullAnnotationsEndpoint
                |> P.setParams
                    [ P.param "annotation_annotator" P.null
                    , P.param "bad_for" P.null
                    , P.order [ P.asc "last_lock" ]
                    , P.limit 1
                    , P.select <| P.attributes imageAnnotationsNoBase64Attrs
                    ]
                |> P.toTask jwt

        extractImage images =
            case images of
                [ image ] ->
                    Just image

                _ ->
                    Nothing
    in
    getTask
        |> Task.map extractImage


countsTask : P.JWT -> Task.Task P.Error Counts
countsTask jwt =
    Task.map2 countsFromPairAndInt
        (P.toTask jwt <| P.getOne imagesCountEndpoint)
        (P.toTask jwt <| P.getOne annotationsCountEndpoint)


posixToSeconds : Time.Posix -> Int
posixToSeconds posix =
    Time.posixToMillis posix // 1000


lockImageTask : P.JWT -> Image -> Task.Task P.Error Image
lockImageTask jwt image =
    let
        patchTask now =
            E.object [ ( "last_lock", E.int <| posixToSeconds now ) ]
                |> P.patchByPrimaryKey imagesEndpoint primaryKey image.id
                |> P.toTask jwt
    in
    Time.now
        |> Task.andThen patchTask


patchImageBadState : Image -> P.Request Image
patchImageBadState image =
    E.object [ ( "bad_for", ME.unwrap E.null E.string image.badFor ) ]
        |> P.patchByPrimaryKey imagesEndpoint primaryKey image.id
        |> P.setParams [ P.select <| P.attributes imageNoBase64Attrs ]


patchAnnotation : P.JWT -> Annotation -> Task.Task P.Error Annotation
patchAnnotation jwt annotation =
    let
        patchTask now =
            E.object [ ( "points", encodePoints annotation.points ), ( "created_at", E.int <| posixToSeconds now ) ]
                |> P.patchByPrimaryKey annotationsEndpoint primaryKey annotation.id
                |> P.toTask jwt
    in
    Time.now
        |> Task.andThen patchTask


postAnnotation : Image -> Points -> P.Request Annotation
postAnnotation image points =
    E.object [ ( "points", encodePoints points ), ( "image", E.int image.id ) ]
        |> P.postOne annotationsEndpoint


pointToString : Point -> String
pointToString point =
    "(" ++ String.fromInt point.x ++ "," ++ String.fromInt point.y ++ ")"


encodePoints : Points -> E.Value
encodePoints points =
    "("
        ++ (points |> pointsToList |> List.map pointToString |> String.join ",")
        ++ ")"
        |> E.string


decodeCount : D.Decoder Int
decodeCount =
    D.field "count" D.int


decodeImagesCount : D.Decoder ( Int, Int )
decodeImagesCount =
    D.map2 Tuple.pair
        (D.field "all" D.int)
        (D.field "non_bad" D.int)


decodeImageAnnotation : D.Decoder ImageAnnotation
decodeImageAnnotation =
    D.map2 ImageAnnotation
        decodeImage
        (decodeAnnotationWithPrefix "annotation_")


decodeAnnotation : D.Decoder Annotation
decodeAnnotation =
    decodeAnnotationWithPrefix ""


decodeAnnotationWithPrefix : String -> D.Decoder Annotation
decodeAnnotationWithPrefix prefix =
    D.map4 Annotation
        (D.field (prefix ++ "id") D.int)
        (D.field (prefix ++ "annotator") D.string)
        (D.field (prefix ++ "points") decodePoints)
        (D.field (prefix ++ "created_at") decodeTime)


decodeImage : D.Decoder Image
decodeImage =
    D.map5 Image
        (D.field "id" D.int)
        (D.field "path" D.string)
        (D.maybe <| D.field "base64" D.string)
        (D.field "bad_for" (D.nullable D.string))
        (D.field "last_lock" (D.nullable decodeTime))


decodeTime : D.Decoder Time.Posix
decodeTime =
    D.map (\seconds -> Time.millisToPosix (1000 * seconds)) D.int


decodeInt : Parser.Parser Int
decodeInt =
    Parser.oneOf
        [ Parser.succeed negate
            |. Parser.symbol "-"
            |= Parser.int
        , Parser.int
        ]


decodePoints : D.Decoder Points
decodePoints =
    D.string
        |> D.andThen decodePointsHelp


decodePointsHelp : String -> D.Decoder Points
decodePointsHelp string =
    case Parser.run parsePoints string of
        Ok points ->
            D.succeed points

        Err deadEnds ->
            D.fail (Parser.deadEndsToString deadEnds)


parsePoint : Parser.Parser Point
parsePoint =
    Parser.succeed Point
        |. Parser.symbol "("
        |. Parser.spaces
        |= decodeInt
        |. Parser.spaces
        |. Parser.symbol ","
        |. Parser.spaces
        |= decodeInt
        |. Parser.spaces
        |. Parser.symbol ")"


parsePoints : Parser.Parser Points
parsePoints =
    Parser.succeed Points
        |. Parser.symbol "("
        |. Parser.spaces
        |= parsePoint
        |. Parser.spaces
        |. Parser.symbol ","
        |. Parser.spaces
        |= parsePoint
        |. Parser.spaces
        |. Parser.symbol ","
        |. Parser.spaces
        |= parsePoint
        |. Parser.spaces
        |. Parser.symbol ","
        |. Parser.spaces
        |= parsePoint
        |. Parser.spaces
        |. Parser.symbol ")"



-- MODEL


pointsToList : Points -> List Point
pointsToList points =
    [ points.p1, points.p2, points.p3, points.p4 ]


pointsToEZipper : Points -> EZipper Point
pointsToEZipper =
    pointsToList >> EZipper.fromListSelectHead


ezipperToPoints : EZipper Point -> Maybe Points
ezipperToPoints ezipper =
    case EZipper.toList ezipper of
        [ p1, p2, p3, p4 ] ->
            Just <| Points p1 p2 p3 p4

        _ ->
            Nothing


type alias Size =
    { width : Int
    , height : Int
    }


type alias Token =
    String


type LoggingModel
    = LoggedOut String
    | LoggingIn Token
    | LoggedIn Token LoggedModel


type alias EditorModel =
    { points : EZipper Point
    , trackingOffset : Maybe Point
    , transaction : EditorTransaction
    }


type EditorTransaction
    = Standby
    | InFlight


emptyEditor : EditorModel
emptyEditor =
    { points = EZipper.empty, trackingOffset = Nothing, transaction = Standby }


type alias LoggedModel =
    { selector : Selector
    , editor : EditorModel
    , counts : Maybe Counts
    }


type alias Counts =
    { images : Int, imagesNonBad : Int, annotations : Int }


countsFromPairAndInt : ( Int, Int ) -> Int -> Counts
countsFromPairAndInt ( imagesAll, imagesNonBad ) annotations =
    Counts imagesAll imagesNonBad annotations


type alias Model =
    { windowSize : Size
    , token : Token
    , loggingModel : LoggingModel
    }


initModel : Size -> Model
initModel size =
    Model size "" (LoggedOut "")



-- MESSAGES


type Msg
    = NoOp
    | Logout
    | Login String
    | SetToken String
    | GotBadAndAnnotatedImages (Result P.Error ( List Image, List ImageAnnotation ))
    | GotEditImage (Result P.Error (Maybe Image))
    | GotCounts (Result P.Error Counts)
    | SelectBad Int
    | SelectAnnotated Int
    | SelectEdit
    | GotImage (Result P.Error Image)
    | AddOrModifyPoint Bool Point Float
    | MovePoint Point
    | ReleasePoint
    | ToggleMarkedBad
    | SaveAnnotation



-- UPDATE


postgrestErrorToString : P.PostgrestErrorJSON -> String
postgrestErrorToString error =
    "postgrest_message: "
        ++ Maybe.withDefault "none" error.message
        ++ "\npostgrest_details: "
        ++ Maybe.withDefault "none" error.details
        ++ "\npostgrest_hint: "
        ++ Maybe.withDefault "none" error.hint
        ++ "\npostgrest_code: "
        ++ Maybe.withDefault "none" error.code


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        Logout ->
            ( Model model.windowSize "" (LoggedOut "Vous êtes bien déconnecté"), Cmd.none )

        Login user ->
            let
                jwt =
                    P.jwt model.token
            in
            ( { model | loggingModel = LoggingIn user }
            , userImagesBadAndAnnotatedImagesTask jwt user
                |> Task.attempt GotBadAndAnnotatedImages
            )

        SetToken token ->
            ( { model | token = token, loggingModel = LoggedOut "" }, Cmd.none )

        GotBadAndAnnotatedImages result ->
            let
                jwt =
                    P.jwt model.token

                maybeLock maybeImage =
                    case maybeImage of
                        Nothing ->
                            Task.succeed Nothing

                        Just image ->
                            lockImageTask jwt image
                                |> Task.map Just

                newLoggedInModel user bads annotated counts =
                    ( { model
                        | loggingModel =
                            LoggedIn user
                                { selector = makeSelectorAtEdit bads annotated Nothing
                                , editor = emptyEditor
                                , counts = counts
                                }
                      }
                    , Cmd.batch
                        [ unannotatedImageTask jwt
                            |> Task.andThen maybeLock
                            |> Task.attempt GotEditImage
                        , countsTask jwt
                            |> Task.attempt GotCounts
                        ]
                    )
            in
            case ( model.loggingModel, result ) of
                ( LoggedOut _, _ ) ->
                    ( model, Cmd.none )

                ( LoggingIn user, Ok ( bads, annotated ) ) ->
                    newLoggedInModel user bads annotated Nothing

                ( LoggingIn _, Err error ) ->
                    ( { model | loggingModel = LoggedOut <| errorText error }, Cmd.none )

                ( LoggedIn user loggedModel, Ok ( bads, annotated ) ) ->
                    newLoggedInModel user bads annotated loggedModel.counts

                ( LoggedIn _ _, Err error ) ->
                    ( { model | loggingModel = LoggedOut <| errorText error }, Cmd.none )

        GotEditImage result ->
            loggedInUpdate model <|
                \user loggedModel ->
                    case result of
                        Ok (Just image) ->
                            ( LoggedIn user { loggedModel | selector = setEdit image loggedModel.selector }
                            , Cmd.none
                            )

                        Ok Nothing ->
                            ( LoggedIn user loggedModel
                            , Cmd.none
                            )

                        Err error ->
                            ( LoggedOut <| errorText error
                            , Cmd.none
                            )

        GotCounts result ->
            loggedInUpdate model <|
                \user loggedModel ->
                    case result of
                        Ok counts ->
                            ( LoggedIn user { loggedModel | counts = Just counts }
                            , Cmd.none
                            )

                        Err error ->
                            ( LoggedOut <| errorText error
                            , Cmd.none
                            )

        SelectBad selectImageId ->
            loggedInUpdate model <|
                \user loggedModel ->
                    let
                        newSelector =
                            loggedModel.selector
                                |> selectBad selectImageId
                    in
                    ( LoggedIn user { loggedModel | selector = newSelector, editor = emptyEditor }
                    , ME.unwrap Cmd.none (updateBase64 <| P.jwt model.token) (selectedImage newSelector)
                    )

        SelectAnnotated selectAnnotationId ->
            loggedInUpdate model <|
                \user loggedModel ->
                    let
                        newSelector =
                            loggedModel.selector
                                |> selectAnnotated selectAnnotationId

                        newEditor =
                            selectedAnnotation newSelector
                                |> ME.unwrap loggedModel.editor
                                    (\a -> { points = pointsToEZipper a.points, trackingOffset = Nothing, transaction = Standby })
                    in
                    ( LoggedIn user { loggedModel | selector = newSelector, editor = newEditor }
                    , ME.unwrap Cmd.none (updateBase64 <| P.jwt model.token) (selectedImage newSelector)
                    )

        SelectEdit ->
            loggedInUpdate model <|
                \user loggedModel ->
                    let
                        newSelector =
                            selectEdit loggedModel.selector
                    in
                    ( LoggedIn user { loggedModel | selector = newSelector, editor = emptyEditor }
                    , ME.unwrap Cmd.none (updateBase64 <| P.jwt model.token) (selectedImage newSelector)
                    )

        GotImage result ->
            loggedInUpdate model <|
                \user loggedModel ->
                    case result of
                        Ok image ->
                            ( LoggedIn user { loggedModel | selector = updateImage image loggedModel.selector }
                            , Cmd.none
                            )

                        Err error ->
                            ( LoggedOut <| errorText error
                            , Cmd.none
                            )

        AddOrModifyPoint remove loc radius ->
            loggedInUpdate model <|
                \user loggedModel ->
                    let
                        maybeTarget =
                            loggedModel.editor.points
                                |> EZipper.toList
                                |> firstPointInCircle loc radius
                    in
                    case ( remove, maybeTarget ) of
                        ( False, Just target ) ->
                            let
                                newPoints =
                                    loggedModel.editor.points
                                        |> EZipper.selectWhen (\p -> p == target)

                                diff =
                                    pointDiff loc target

                                editor =
                                    loggedModel.editor

                                newEditor =
                                    { editor | points = newPoints, trackingOffset = Just diff }
                            in
                            ( LoggedIn user { loggedModel | editor = newEditor }
                            , Cmd.none
                            )

                        ( False, Nothing ) ->
                            let
                                newPoints =
                                    EZipper.fromParts [] loc (EZipper.toList loggedModel.editor.points)

                                editor =
                                    loggedModel.editor

                                newEditor =
                                    { editor | points = newPoints, trackingOffset = Just <| Point 0 0 }
                            in
                            ( LoggedIn user { loggedModel | editor = newEditor }
                            , Cmd.none
                            )

                        ( True, Just target ) ->
                            let
                                newPoints =
                                    loggedModel.editor.points
                                        |> EZipper.filterAndReset (\p -> p /= target)

                                editor =
                                    loggedModel.editor

                                newEditor =
                                    { editor | points = newPoints, trackingOffset = Nothing }
                            in
                            ( LoggedIn user { loggedModel | editor = newEditor }
                            , Cmd.none
                            )

                        ( True, Nothing ) ->
                            ( LoggedIn user loggedModel
                            , Cmd.none
                            )

        MovePoint loc ->
            loggedInUpdate model <|
                \user loggedModel ->
                    case ( EZipper.selection loggedModel.editor.points, loggedModel.editor.trackingOffset ) of
                        ( Just target, Just locToTarget ) ->
                            let
                                editor =
                                    loggedModel.editor

                                newPoints =
                                    editor.points
                                        |> EZipper.map
                                            (\p ->
                                                if p == target then
                                                    pointAdd loc locToTarget

                                                else
                                                    p
                                            )

                                newEditor =
                                    { editor | points = newPoints }
                            in
                            ( LoggedIn user { loggedModel | editor = newEditor }
                            , Cmd.none
                            )

                        _ ->
                            ( LoggedIn user loggedModel
                            , Cmd.none
                            )

        ReleasePoint ->
            loggedInUpdate model <|
                \user loggedModel ->
                    let
                        editor =
                            loggedModel.editor

                        newEditor =
                            { editor | trackingOffset = Nothing }
                    in
                    ( LoggedIn user { loggedModel | editor = newEditor }
                    , Cmd.none
                    )

        ToggleMarkedBad ->
            loggedInUpdate model <|
                \user loggedModel ->
                    case selectedImage loggedModel.selector of
                        Just image ->
                            let
                                jwt =
                                    P.jwt model.token

                                newImage =
                                    if image.badFor == Nothing then
                                        { image | badFor = Just user }

                                    else
                                        { image | badFor = Nothing }

                                editor =
                                    loggedModel.editor

                                newEditor =
                                    { editor | transaction = InFlight }
                            in
                            ( LoggedIn user { loggedModel | editor = newEditor }
                            , patchImageBadState newImage
                                |> P.toTask jwt
                                |> Task.andThen (\_ -> userImagesBadAndAnnotatedImagesTask jwt user)
                                |> Task.attempt GotBadAndAnnotatedImages
                            )

                        Nothing ->
                            ( LoggedIn user loggedModel
                            , Cmd.none
                            )

        SaveAnnotation ->
            loggedInUpdate model <|
                \user loggedModel ->
                    case ezipperToPoints loggedModel.editor.points of
                        Just editorPoints ->
                            let
                                jwt =
                                    P.jwt model.token

                                editor =
                                    loggedModel.editor

                                newEditor =
                                    { editor | transaction = InFlight }
                            in
                            case ( selectedAnnotation loggedModel.selector, selectedImage loggedModel.selector ) of
                                ( Just annotation, _ ) ->
                                    ( LoggedIn user { loggedModel | editor = newEditor }
                                    , patchAnnotation jwt { annotation | points = editorPoints }
                                        |> Task.andThen (\_ -> userImagesBadAndAnnotatedImagesTask jwt user)
                                        |> Task.attempt GotBadAndAnnotatedImages
                                    )

                                ( Nothing, Just image ) ->
                                    ( LoggedIn user { loggedModel | editor = newEditor }
                                    , postAnnotation image editorPoints
                                        |> P.toTask jwt
                                        |> Task.andThen (\_ -> userImagesBadAndAnnotatedImagesTask jwt user)
                                        |> Task.attempt GotBadAndAnnotatedImages
                                    )

                                ( Nothing, Nothing ) ->
                                    ( LoggedIn user loggedModel
                                    , Cmd.none
                                    )

                        Nothing ->
                            ( LoggedIn user loggedModel
                            , Cmd.none
                            )


updateBase64 : P.JWT -> Image -> Cmd Msg
updateBase64 jwt image =
    case image.base64 of
        Nothing ->
            getImage image.id
                |> P.toCmd jwt GotImage

        Just _ ->
            Cmd.none


pointDiff : Point -> Point -> Point
pointDiff p1 p2 =
    Point (p2.x - p1.x) (p2.y - p1.y)


pointAdd : Point -> Point -> Point
pointAdd p1 p2 =
    Point (p1.x + p2.x) (p1.y + p2.y)


pointDistance : Point -> Point -> Float
pointDistance p1 p2 =
    ((p2.x - p1.x) ^ 2)
        + ((p2.y - p1.y) ^ 2)
        |> toFloat
        |> sqrt


firstPointInCircle : Point -> Float -> List Point -> Maybe Point
firstPointInCircle center radius points =
    points
        |> List.filter (\p -> pointDistance center p <= radius)
        |> List.head


loggedInUpdate : Model -> (String -> LoggedModel -> ( LoggingModel, Cmd Msg )) -> ( Model, Cmd Msg )
loggedInUpdate model func =
    case model.loggingModel of
        LoggedIn user loggedModel ->
            let
                ( newLoggingModel, cmd ) =
                    func user loggedModel
            in
            ( { model | loggingModel = newLoggingModel }, cmd )

        _ ->
            ( model, Cmd.none )


errorText : P.Error -> String
errorText error =
    case error of
        P.Timeout ->
            "Je n'arrive pas à accéder au serveur (temps limite écoulé). Vous êtes connecté à internet ?"

        P.BadUrl errorMsg ->
            "Un problème avec l'application est survenu, merci de contacter les développeurs.\nMessage : " ++ errorMsg

        P.NetworkError ->
            "Je n'arrive pas à accéder au serveur (erreur réseau). Êtes-vous connecté à internet ?"

        P.BadStatus code errorMsg postgrestError ->
            if code == 401 then
                if String.contains "JWT expired" errorMsg then
                    "Votre jeton a expiré, écrivez à Sébastien pour en obtenir un nouveau."

                else
                    "Le serveur refuse votre jeton d'authentification. L'avez-vous bien copié-collé sans erreurs ?"

            else
                let
                    fullErrorMsg =
                        "code: "
                            ++ String.fromInt code
                            ++ "\nmessage: "
                            ++ errorMsg
                            ++ "\n"
                            ++ postgrestErrorToString postgrestError
                in
                "Un problème avec l'application est survenu, merci de contacter les développeurs.\nMessage : " ++ errorMsg

        P.BadBody errorMsg ->
            "Un problème avec l'application est survenu, merci de contacter les développeurs.\nMessage : " ++ errorMsg



-- VIEW


tokenUser : String -> Result Jwt.JwtError String
tokenUser token =
    let
        decoder =
            D.field "user" D.string
    in
    decodeToken decoder token


view : Model -> El.Element Msg
view { windowSize, token, loggingModel } =
    case loggingModel of
        LoggedOut error ->
            let
                state =
                    case tokenUser token of
                        Ok user ->
                            ParsedToken user

                        Err _ ->
                            if String.length token > 0 then
                                InvalidToken

                            else
                                EmptyToken
            in
            loginView token error state

        LoggingIn user ->
            loginView token "" (LoggingToken user)

        LoggedIn user loggedModel ->
            El.row
                [ El.padding 20
                , El.spacing 20
                ]
                [ El.column
                    [ El.width <| El.minimum 300 El.shrink
                    , El.height <| El.px (windowSize.height - 40)
                    , El.spacing 20

                    -- , El.explain Debug.todo
                    ]
                    [ El.column
                        [ El.width El.fill
                        , El.height <| El.px 100
                        , El.spacing 10
                        , Border.width 3
                        , Border.color blue
                        ]
                        [ El.paragraph [ Background.color blue, El.paddingXY 0 5, Font.center ] [ El.text "Annotation à faire" ]
                        , editImageView (isEditSelected loggedModel.selector) (selectorEdit loggedModel.selector)
                        ]
                    , El.column
                        [ El.width El.fill
                        , El.height <| El.px <| round <| toFloat (windowSize.height - 80 - 100) * 0.6
                        , El.spacing 10
                        , Border.width 3
                        , Border.color green
                        ]
                        [ El.paragraph [ Background.color green, El.paddingXY 0 5, Font.center ]
                            [ El.text <| String.fromInt (lengthAnnotated loggedModel.selector) ++ " annotations réalisées" ]
                        , annotatedImagesView (isAnnotatedSelected loggedModel.selector) (selectorAnnotated loggedModel.selector)
                        ]
                    , El.column
                        [ El.width El.fill
                        , El.height <| El.px <| round <| toFloat (windowSize.height - 80 - 100) * 0.4
                        , El.scrollbarY
                        , El.spacing 10
                        , Border.width 3
                        , Border.color red
                        ]
                        [ El.paragraph [ Background.color red, El.paddingXY 0 5, Font.center ]
                            [ El.text <| String.fromInt (lengthBad loggedModel.selector) ++ " mauvaises images" ]
                        , badImagesView (isBadSelected loggedModel.selector) (selectorBad loggedModel.selector)
                        ]
                    ]
                , El.column [ El.alignTop, El.width <| El.px svgWidth, El.spacing 30 ]
                    [ El.row [ El.width El.fill ]
                        [ countsView loggedModel.counts
                        , El.row [ El.alignRight, El.spacing 30 ]
                            [ El.paragraph [] [ El.text "Loggué en tant que ", El.el [ Font.bold ] <| El.text user ]
                            , button False Logout (El.text "Se déconnecter")
                            ]
                        ]
                    , intsructionsView
                    , editorView loggedModel
                    ]
                ]


countsView : Maybe Counts -> El.Element Msg
countsView maybeCounts =
    let
        contents =
            case maybeCounts of
                Nothing ->
                    []

                Just counts ->
                    [ El.text <| String.fromInt counts.images ++ " images sur le serveur"
                    , El.text <| String.fromInt counts.imagesNonBad ++ " images non-mauvaises"
                    , El.text <| String.fromInt counts.annotations ++ " annotations réalisées par l'ensemble des participants"
                    ]
    in
    El.column [ El.spacing 5, Font.size 16 ] contents


editorView : LoggedModel -> El.Element Msg
editorView loggedModel =
    case selectedImage loggedModel.selector of
        Nothing ->
            El.text "Pas d'image sélectionnée"

        Just image ->
            case image.base64 of
                Nothing ->
                    El.text "Chargement de l'image..."

                Just base64 ->
                    case imageSize base64 of
                        Nothing ->
                            El.text "Encodage de l'image invalide"

                        Just size ->
                            El.column [ El.alignTop, El.spacing 30 ]
                                [ editorViewButtons loggedModel
                                , El.el [] <| El.html <| editorViewSvg (not <| isBadSelected loggedModel.selector) size base64 loggedModel.editor
                                ]


intsructionsView : El.Element Msg
intsructionsView =
    El.column [ El.spacing 20 ]
        [ El.el [ Font.heavy, Font.size 30 ] <| El.text "Instructions"
        , El.row [ Font.size 16 ]
            [ El.textColumn [ El.alignTop, El.spacing 20, El.width El.fill ]
                [ El.paragraph []
                    [ El.text "Dessiner le quadrilatère que forme la carte d'identité. Ne détourer que "
                    , El.el [ Font.bold ] <| El.text "la partie opaque"
                    , El.text " (la marge transparente reste en dehors du quadrilatère)."
                    ]
                , El.paragraph []
                    [ El.text "Cliquer pour ajouter un point. Glisser-déplacer l'intérieur d'un cercle jaune pour le déplacer. Shift-cliquer dans un cercle jaune pour le supprimer. Lorsque vous êtes satisfait du quadrilatère, cliquez sur "
                    , El.el [ Font.italic ] <| El.text "Sauver"
                    , El.text ". L'image passe alors dans la liste verte, à gauche, et vous pouvez annoter l'image suivante."
                    ]
                ]
            , El.paragraph [ El.alignTop ]
                [ El.text "Si l'image est trop floue, ou si la carte d'identité sort du cadre, cliquez sur "
                , El.el [ Font.italic ] <| El.text "L'image est mauvaise"
                , El.text ". L'image passe alors dans le cadre rouge, et vous pouvez annoter l'image suivante. On peut toujours éditer les annotations précédentes, ou récupérer une \"image mauvaise\" en cliquant dans les listes à gauche."
                ]
            ]
        ]


editorViewButtons : LoggedModel -> El.Element Msg
editorViewButtons { selector, editor } =
    let
        nPointsText =
            if EZipper.length editor.points /= 4 then
                "Cliquez sur l'image pour avoir exactement 4 points"

            else
                ""
    in
    case selectorSelection selector of
        Bad ->
            button (editor.transaction == InFlight) ToggleMarkedBad (El.text "L'image est bonne")

        Annotated ->
            El.row [ El.spacing 30 ]
                [ button
                    ((editor.transaction == InFlight) || (EZipper.length editor.points /= 4))
                    SaveAnnotation
                    (El.text "Mettre à jour l'annotation")
                , button
                    (editor.transaction == InFlight)
                    ToggleMarkedBad
                    (El.text "L'image est mauvaise")
                , El.text nPointsText
                ]

        Edit ->
            El.row [ El.spacing 30 ]
                [ button
                    ((editor.transaction == InFlight) || (EZipper.length editor.points /= 4))
                    SaveAnnotation
                    (El.text "Sauver l'annotation")
                , button
                    (editor.transaction == InFlight)
                    ToggleMarkedBad
                    (El.text "L'image est mauvaise")
                , El.text nPointsText
                ]


imageSize : String -> Maybe Size
imageSize base64 =
    -- Skip the first 16 bytes, and get bytes 16-24 which encode width and height
    -- see https://stackoverflow.com/questions/15327959/get-height-and-width-dimensions-from-base64-png
    let
        decoder =
            BD.map3 (\_ w h -> Size w h)
                (BD.bytes 16)
                (BD.unsignedInt32 Bytes.BE)
                (BD.unsignedInt32 Bytes.BE)
    in
    String.slice 0 32 base64
        |> Base64.toBytes
        |> ME.unwrap Nothing (BD.decode decoder)


scalePoint : Size -> Size -> Point -> Point
scalePoint oldSize newSize { x, y } =
    { x = round <| toFloat x * toFloat newSize.width / toFloat oldSize.width
    , y = round <| toFloat y * toFloat newSize.height / toFloat oldSize.height
    }


onMouseDown : (Bool -> Point -> msg) -> Svg.Attribute msg
onMouseDown message =
    SE.on "mousedown" <|
        D.map2 message
            (D.field "shiftKey" D.bool)
            (D.map2 Point
                (D.field "offsetX" D.int)
                (D.field "offsetY" D.int)
            )


decodeToInt : String -> D.Decoder Int
decodeToInt string =
    case String.toInt string of
        Nothing ->
            D.fail "Not parsable as an Int"

        Just n ->
            D.succeed n


onMouseMove : (Point -> msg) -> Svg.Attribute msg
onMouseMove message =
    SE.on "mousemove" <|
        D.map2 (\x y -> message <| Point x y)
            (D.field "offsetX" D.int)
            (D.field "offsetY" D.int)


onMouseUp : msg -> Svg.Attribute msg
onMouseUp message =
    SE.on "mouseup" (D.succeed message)


onDragPreventDefault : Svg.Attribute Msg
onDragPreventDefault =
    SE.preventDefaultOn "dragstart" (D.map alwaysPreventDefault (D.succeed NoOp))


alwaysPreventDefault : msg -> ( msg, Bool )
alwaysPreventDefault msg =
    ( msg, True )


svgWidth : Int
svgWidth =
    1000


editorViewSvg : Bool -> Size -> String -> EditorModel -> Svg.Svg Msg
editorViewSvg active imgSize base64 { points, trackingOffset } =
    let
        ratio =
            toFloat imgSize.height / toFloat imgSize.width

        svgHeight =
            round <| toFloat svgWidth * ratio

        svgSize =
            { width = svgWidth, height = svgHeight }

        widthString =
            String.fromInt svgWidth

        heightString =
            String.fromInt svgHeight

        svgRadius =
            toFloat svgWidth / 20

        imgRadius =
            toFloat imgSize.width / 20

        circle point =
            let
                scaledPoint =
                    scalePoint imgSize svgSize point
            in
            Svg.circle
                [ SA.cx <| String.fromInt scaledPoint.x
                , SA.cy <| String.fromInt scaledPoint.y
                , SA.r <| String.fromInt <| round <| svgRadius
                , SA.fillOpacity "0"
                , SA.strokeWidth "3"
                , SA.stroke "yellow"
                , SA.style "pointer-events: none"
                ]
                []

        circlePoints =
            EZipper.map circle points
                |> EZipper.toList

        polygonPoints =
            points
                |> EZipper.map (scalePoint imgSize svgSize)
                |> EZipper.map (\{ x, y } -> String.fromInt x ++ " " ++ String.fromInt y)
                |> EZipper.toList
                |> String.join ", "
    in
    Svg.svg
        ([ SA.width widthString
         , SA.height heightString
         ]
            ++ (if active then
                    [ onMouseDown <| \shift loc -> AddOrModifyPoint shift (scalePoint svgSize imgSize loc) imgRadius
                    , onMouseUp ReleasePoint
                    ]
                        ++ (if trackingOffset /= Nothing then
                                [ onMouseMove (MovePoint << scalePoint svgSize imgSize) ]

                            else
                                []
                           )

                else
                    []
               )
        )
        ([ Svg.image
            [ SA.width widthString
            , SA.height heightString
            , SA.xlinkHref ("data:image/png;base64," ++ base64)
            , onDragPreventDefault
            ]
            []
         , Svg.polygon
            [ SA.points polygonPoints
            , SA.fillOpacity "0"
            , SA.strokeWidth "3"
            , SA.stroke "orange"
            , SA.style "pointer-events: none"
            ]
            []
         ]
            ++ circlePoints
        )


annotatedImagesView : Bool -> EZipper ImageAnnotation -> El.Element Msg
annotatedImagesView selected annotatedImages =
    case EZipper.selection annotatedImages of
        Nothing ->
            El.paragraph [ El.paddingXY 5 0 ]
                [ El.text "Les images annotées appaîtront ici" ]

        Just sel ->
            annotatedImages
                |> EZipper.map
                    (\ia ->
                        imageView (selected && (ia.annotation.id == sel.annotation.id))
                            (SelectAnnotated ia.annotation.id)
                            ia.image
                    )
                |> EZipper.toList
                |> El.column [ El.height El.fill, El.width El.fill, El.scrollbarY, El.spacing 5, El.paddingXY 5 0 ]


badImagesView : Bool -> EZipper Image -> El.Element Msg
badImagesView selected badImages =
    case EZipper.selection badImages of
        Nothing ->
            El.paragraph [ El.paddingXY 5 0 ]
                [ El.text "Les images marquées comme mauvaises appaîtront ici" ]

        Just sel ->
            badImages
                |> EZipper.map
                    (\image -> imageView (selected && (image.id == sel.id)) (SelectBad image.id) image)
                |> EZipper.toList
                |> El.column [ El.height El.fill, El.width El.fill, El.scrollbarY, El.spacing 5, El.paddingXY 5 0 ]


editImageView : Bool -> Maybe Image -> El.Element Msg
editImageView selected maybeImage =
    case maybeImage of
        Nothing ->
            El.paragraph [ El.width El.fill, El.paddingXY 5 0 ]
                [ El.text "Pas d'image à annoter" ]

        Just image ->
            El.el [ El.width El.fill, El.paddingXY 5 0 ] <| imageView selected SelectEdit image


imageView : Bool -> msg -> Image -> El.Element msg
imageView selected selectMsg image =
    selectionButton selected selectMsg (El.text image.path)


type LoginViewParsedState
    = EmptyToken
    | InvalidToken
    | ParsedToken String
    | LoggingToken String


onEnter : msg -> El.Attribute msg
onEnter msg =
    El.htmlAttribute
        (HE.on "keyup"
            (D.field "key" D.string
                |> D.andThen
                    (\key ->
                        if key == "Enter" then
                            D.succeed msg

                        else
                            D.fail "Not the enter key"
                    )
            )
        )


loginView : String -> String -> LoginViewParsedState -> El.Element Msg
loginView token error parsedState =
    let
        fields =
            case parsedState of
                EmptyToken ->
                    { user = ""
                    , userText = ""
                    , buttonDisabled = True
                    , buttonText = "Entrer"
                    }

                InvalidToken ->
                    { user = ""
                    , userText = "Ce jeton n'est pas valide. Avez-vous bien copié-collé le jeton sans ajouter d'espaces ou supprimer des caractères?"
                    , buttonDisabled = True
                    , buttonText = "Entrer"
                    }

                ParsedToken user ->
                    { user = user
                    , userText = "Bienvenue " ++ user
                    , buttonDisabled = False
                    , buttonText = "Entrer"
                    }

                LoggingToken user ->
                    { user = user
                    , userText = "Bienvenue " ++ user
                    , buttonDisabled = True
                    , buttonText = "Chargement..."
                    }
    in
    El.column [ El.centerX, El.centerY, El.spacing 30, El.width <| El.maximum 700 El.fill ]
        [ Input.text
            [ Input.focusedOnLoad
            , El.spacing 30
            , onEnter <|
                if fields.buttonDisabled then
                    NoOp

                else
                    Login fields.user
            ]
            { onChange = SetToken
            , text = token
            , placeholder = Just <| Input.placeholder [] <| El.text "eyJhbGciOi..."
            , label =
                Input.labelAbove [] <|
                    El.paragraph [] [ El.text "Copiez-collez votre jeton d'accès pour commencer à annoter :" ]
            }
        , El.row [ El.spacing 30 ]
            [ button fields.buttonDisabled (Login fields.user) (El.text fields.buttonText)
            , El.paragraph [] [ El.text fields.userText ]
            ]
        , El.text error
        ]



-- VIEW UTILS


blue : El.Color
blue =
    El.rgb 0.8 0.8 1


red : El.Color
red =
    El.rgb 1 0.8 0.8


green : El.Color
green =
    El.rgb 0.8 1 0.8


grey : El.Color
grey =
    El.rgb 0.5 0.5 0.5


white : El.Color
white =
    El.rgb 1 1 1


lightGray : El.Color
lightGray =
    El.rgb255 204 204 204


darkGray : El.Color
darkGray =
    El.rgb255 60 60 60


button : Bool -> msg -> El.Element msg -> El.Element msg
button disabled onPress label =
    let
        attrs =
            if disabled then
                [ Font.color darkGray
                , Border.color (El.rgb255 214 217 221)
                , Background.color (El.rgb255 249 250 251)
                ]

            else
                [ Font.color (El.rgb255 38 92 131)
                , Border.color (El.rgb255 214 217 221)
                , Background.color (El.rgb255 249 250 251)
                , El.mouseOver [ Background.color (El.rgb255 237 240 243) ]
                , El.mouseDown [ Background.color lightGray, Border.color lightGray, Font.color darkGray ]
                ]
    in
    Input.button
        ([ Border.width 1
         , El.padding 8
         ]
            ++ attrs
        )
        { onPress =
            if disabled then
                Nothing

            else
                Just onPress
        , label = label
        }


selectionButton : Bool -> msg -> El.Element msg -> El.Element msg
selectionButton selected onPress label =
    let
        attrs =
            if selected then
                [ Font.color white
                , Border.color (El.rgb255 214 217 221)
                , Background.color (El.rgb255 0 116 217)
                , El.mouseOver
                    [ Background.color (El.rgb255 0 99 170)
                    , Border.color (El.rgb255 0 99 170)
                    ]
                ]

            else
                [ Font.color (El.rgb255 38 92 131)
                , Border.color (El.rgb255 214 217 221)
                , Background.color (El.rgb255 249 250 251)
                , El.mouseOver [ Background.color (El.rgb255 237 240 243) ]
                , El.mouseDown [ Background.color lightGray, Border.color lightGray, Font.color darkGray ]
                ]
    in
    Input.button
        ([ Border.width 1
         , El.paddingXY 2 5
         , El.width El.fill
         ]
            ++ attrs
        )
        { onPress = Just onPress
        , label = label
        }
