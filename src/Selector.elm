module Selector exposing (..)

import EZipper exposing (EZipper)
import Maybe.Extra as ME
import Time


type alias Point =
    { x : Int, y : Int }


type alias Points =
    { p1 : Point
    , p2 : Point
    , p3 : Point
    , p4 : Point
    }


type alias Image =
    { id : Int
    , path : String
    , base64 : Maybe String
    , badFor : Maybe String
    , lastLock : Maybe Time.Posix
    }


type alias Annotation =
    { id : Int
    , annotator : String
    , points : Points
    , createdAt : Time.Posix
    }


type alias ImageAnnotation =
    { image : Image
    , annotation : Annotation
    }


type Selection
    = Bad
    | Annotated
    | Edit


type Selector
    = Selector
        { bad : EZipper Image
        , annotated : EZipper ImageAnnotation
        , edit : Maybe Image
        , selection : Selection
        }


makeSelectorAtEdit : List Image -> List ImageAnnotation -> Maybe Image -> Selector
makeSelectorAtEdit bad annotated edit =
    Selector
        { bad = EZipper.fromListSelectHead bad
        , annotated = EZipper.fromListSelectHead annotated
        , edit = edit
        , selection = Edit
        }


setEdit : Image -> Selector -> Selector
setEdit image (Selector selector) =
    Selector { selector | edit = Just image }


updateImage : Image -> Selector -> Selector
updateImage image (Selector selector) =
    case selector.selection of
        Bad ->
            let
                newBad =
                    selector.bad
                        |> EZipper.map
                            (\i ->
                                if i.id == image.id then
                                    image

                                else
                                    { i | base64 = Nothing }
                            )
            in
            Selector { selector | bad = newBad }

        Annotated ->
            let
                newAnnotated =
                    selector.annotated
                        |> EZipper.map
                            (\ia ->
                                if ia.image.id == image.id then
                                    { ia | image = image }

                                else
                                    let
                                        iaImage =
                                            ia.image

                                        newIaImage =
                                            { iaImage | base64 = Nothing }
                                    in
                                    { ia | image = newIaImage }
                            )
            in
            Selector { selector | annotated = newAnnotated }

        Edit ->
            if ME.unwrap -1 .id selector.edit == image.id then
                Selector { selector | edit = Just image }

            else
                Selector selector


selectBad : Int -> Selector -> Selector
selectBad imageId (Selector selector) =
    if EZipper.isEmpty selector.bad then
        Selector selector

    else
        let
            newBad =
                selector.bad
                    |> EZipper.selectWhen (\i -> i.id == imageId)
        in
        Selector { selector | selection = Bad, bad = newBad }


selectAnnotated : Int -> Selector -> Selector
selectAnnotated annotationId (Selector selector) =
    if EZipper.isEmpty selector.annotated then
        Selector selector

    else
        let
            newAnnotated =
                selector.annotated
                    |> EZipper.selectWhen (\ia -> ia.annotation.id == annotationId)
        in
        Selector { selector | selection = Annotated, annotated = newAnnotated }


selectEdit : Selector -> Selector
selectEdit (Selector selector) =
    if selector.edit == Nothing then
        Selector selector

    else
        Selector { selector | selection = Edit }


selectedImage : Selector -> Maybe Image
selectedImage (Selector selector) =
    case selector.selection of
        Bad ->
            EZipper.selection selector.bad

        Annotated ->
            EZipper.selection selector.annotated
                |> Maybe.map .image

        Edit ->
            selector.edit


selectedAnnotation : Selector -> Maybe Annotation
selectedAnnotation (Selector selector) =
    case selector.selection of
        Annotated ->
            EZipper.selection selector.annotated
                |> Maybe.map .annotation

        _ ->
            Nothing


isBadSelected : Selector -> Bool
isBadSelected (Selector { selection }) =
    selection == Bad


isAnnotatedSelected : Selector -> Bool
isAnnotatedSelected (Selector { selection }) =
    selection == Annotated


isEditSelected : Selector -> Bool
isEditSelected (Selector { selection }) =
    selection == Edit


selectorBad : Selector -> EZipper Image
selectorBad (Selector { bad }) =
    bad


selectorAnnotated : Selector -> EZipper ImageAnnotation
selectorAnnotated (Selector { annotated }) =
    annotated


selectorEdit : Selector -> Maybe Image
selectorEdit (Selector { edit }) =
    edit


selectorSelection : Selector -> Selection
selectorSelection (Selector { selection }) =
    selection


lengthBad : Selector -> Int
lengthBad (Selector { bad }) =
    EZipper.length bad


lengthAnnotated : Selector -> Int
lengthAnnotated (Selector { annotated }) =
    EZipper.length annotated
