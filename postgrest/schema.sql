create schema api;

create table api.images (
  id serial primary key,
  path text unique not null,
  base64 text not null,
  bad_for text,
  last_lock integer
);

create table api.annotations (
  id serial primary key,
  points polygon not null,
  annotator text not null default current_setting('request.jwt.claim.user'),
  created_at integer not null default extract(epoch from now()),
  image integer not null references api.images(id)
);

create policy annotation_policy on api.annotations
  with check (annotator = current_setting('request.jwt.claim.user'));

create view api.images_with_annotations as
          select api.images.id,
                 api.images.path,
                 api.images.base64,
                 api.images.bad_for,
                 api.images.last_lock,
                 api.annotations.id as annotation_id,
                 api.annotations.annotator as annotation_annotator,
                 api.annotations.points as annotation_points,
                 api.annotations.created_at as annotation_created_at
            from api.images
 left outer join api.annotations
              on (api.images.id = api.annotations.image)
        order by api.images.id, annotation_id;

create view api.images_count as
 select count(1) as all,
        count(1) filter (where bad_for is null) as non_bad
   from api.images;
create view api.annotations_count as select count(*) from api.annotations;

drop role authenticator;
create role authenticator noinherit login password 'CHANGETHIS';

drop role annotator_user;
create role annotator_user nologin;
grant annotator_user to authenticator;

grant usage on schema api to annotator_user;

grant select, insert on api.images to annotator_user;
grant update (last_lock, bad_for) on api.images to annotator_user;
grant usage, select on sequence api.images_id_seq to annotator_user;

grant select on api.images_with_annotations to annotator_user;

grant select, insert, update on api.annotations to annotator_user;
grant usage, select on sequence api.annotations_id_seq to annotator_user;

grant select on api.images_count to annotator_user;
grant select on api.annotations_count to annotator_user;
