#!/bin/bash -e

if [ $# != 3 ]; then
    echo "Usage: $(basename $0) secret-file user-name duration-spec"
    exit 1
fi

secret_file=$1
user=$2
duration=$3
exp=$(date -d "+ $duration" +%s)
jwt encode --secret "$(cat $secret_file)" -e $exp -P role=annotator_user -P user=$user
