import os
import sys
import json

import requests


def get_images(baseurl, jwt):
    print('Getting images from server')

    headers = {'Authorization': 'Bearer {}'.format(jwt)}
    r = requests.get(baseurl + '/images',
                     params={'select': 'id,path,bad_for'},
                     headers=headers)
    r.raise_for_status()

    images = {}
    for item in r.json():
        images[item['id']] = {'path': item['path'],
                              'bad_for': item['bad_for']}

    print('Gathered {} images'.format(len(images)))
    return images


def parse_point(string):
    return [int(c) for c in string.split(',')]


def parse_points(string):
    return [parse_point(p)
            for p in string.strip()[2:-2].split('),(')]


def get_annotations(baseurl, jwt):
    print('Getting annotations from server')

    headers = {'Authorization': 'Bearer {}'.format(jwt)}
    r = requests.get(baseurl + '/annotations',
                     params={'select': 'id,image,annotator,points'},
                     headers=headers)
    r.raise_for_status()

    annotations = {}
    for item in r.json():
        annotations[item['id']] = {
            'image': item['image'],
            'annotator': item['annotator'],
            'points': parse_points(item['points'])
        }

    print('Gathered {} annotations'.format(len(annotations)))
    return annotations


def export_annotations(baseurl, jwt, datasetpath, overwrite):
    imagespath = os.path.join(datasetpath, 'images')
    annotationspath = os.path.join(datasetpath, 'ground-truth')

    images = get_images(baseurl, jwt)
    annotations = get_annotations(baseurl, jwt)

    for annotation in annotations.values():
        image = images[annotation['image']]
        imagename = image['path']
        if image['bad_for'] is not None:
            print(' -> Skipping {} marked bad by {}'
                  .format(imagename, image['bad_for']))
            continue

        imagepath = os.path.join(imagespath, imagename)
        if not os.path.exists(imagepath):
            raise ValueError('{} not found in the dataset'.format(imagename))

        apath = os.path.join(annotationspath,
                             imagename.replace('.png', '.json'))
        if os.path.exists(apath):
            if overwrite:
                print(' -> Warning: overwriting {}'.format(apath))
            else:
                print(' -> Warning: skipping {} which already exists'
                      .format(apath))
        else:
            basepath, _ = os.path.split(apath)
            if not os.path.exists(basepath):
                os.mkdir(basepath)

        print(' => saving annotation for {}'.format(imagename))
        with open(apath, 'w') as f:
            json.dump({
                'quad': annotation['points'],
                'annotator': annotation['annotator']
            }, f)


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print("Usage: {} <base-url> <dataset-folder> "
              "<overwrite> <user-jwt>"
              .format(os.path.basename(sys.argv[0])))
        sys.exit(1)

    baseurl, datasetpath, jwt = sys.argv[1], sys.argv[2], sys.argv[4]
    if sys.argv[3] == 'True':
        overwrite = True
    else:
        assert sys.argv[3] == 'False'
        overwrite = False

    if ('https' not in baseurl
            and 'localhost' not in baseurl
            and '127.0.0.1' not in baseurl):
        print('Not a local url and not https! Not getting this '
              'data in cleartext.')
        sys.exit(1)

    print("Downloading annotations to '{}'".format(datasetpath))
    export_annotations(baseurl, jwt, datasetpath, overwrite)
