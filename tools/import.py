import os
import sys
from glob import glob
import base64
import json
import time
from random import randint

import requests


DEFAULT_ANNOTATOR = 'sl'
RETRIES = 5
RETRY_DELAY = 30
MAX_INITIAL_LOCK = 1578843658


def try_request(method, *args, **kwargs):
    for _ in range(RETRIES):
        r = method(*args, **kwargs)
        if r.status_code < 500:
            return r
        else:
            print('Request failed with code {}, retrying in {} seconds...'
                  .format(r.status_code, RETRY_DELAY))
            time.sleep(RETRY_DELAY)

    raise ValueError('Request failed {} times'.format(RETRIES))


def get_imagenames(baseurl, jwt):
    print('Getting image names from server')

    headers = {'Authorization': 'Bearer {}'.format(jwt)}
    r = requests.get(baseurl + '/images',
                     params={'select': 'id,path'},
                     headers=headers)
    r.raise_for_status()

    images = {}
    for item in r.json():
        images[item['path']] = item['id']

    print('Gathered {} existing images on the server'.format(len(images)))
    return images


def upload_image(baseurl, jwt, imagespath, imagename, logprefix=''):
    print(logprefix + 'uploading image {}'.format(imagename))

    headers = {'Authorization': 'Bearer {}'.format(jwt)}
    data = {'path': imagename, 'last_lock': randint(1, MAX_INITIAL_LOCK)}
    with open(imagespath + '/' + imagename, 'rb') as f:
        data['base64'] = base64.b64encode(f.read()).decode()

    r = try_request(requests.post, baseurl + '/images', json=data,
                    headers=headers)
    r.raise_for_status()

    location = r.headers['Location']
    return int(location[location.find('?id=eq.') + 7:])


def encode_point(point):
    return '(' + ','.join(map(str, point)) + ')'


def encode_points(points):
    return '(' + ','.join([encode_point(p) for p in points]) + ')'


def upload_annotation(baseurl, jwt, annotationspath, annotationname, image_id,
                      logprefix=''):
    print(logprefix + 'uploading annotation {}'.format(annotationname))

    with open(annotationspath + '/' + annotationname, 'r') as f:
        annotation = json.load(f)

    headers = {'Authorization': 'Bearer {}'.format(jwt)}
    annotator = annotation.get('annotator', DEFAULT_ANNOTATOR)
    r = try_request(requests.get, baseurl + '/annotations',
                    params={'image': 'eq.{}'.format(image_id),
                            'annotator': 'eq.' + annotator,
                            'limit': 1},
                    headers=headers)
    if r.status_code == 200 and len(r.json()) > 0:
        print(logprefix + 'already on the server, nothing to do')
        return

    data = {
        'points': encode_points(annotation['quad']),
        'annotator': annotator,
        'image': image_id
    }
    r = try_request(requests.post, baseurl + '/annotations', json=data,
                    headers=headers)
    r.raise_for_status()


def upload_dataset(baseurl, jwt, datasetpath):
    imagenames = get_imagenames(baseurl, jwt)
    for imagepath in glob(os.path.join(datasetpath, 'images/*/*.png')):
        basepath, imagename = os.path.split(imagepath)
        basepath, author = os.path.split(basepath)
        basepath, _ = os.path.split(basepath)
        imagename = author + '/' + imagename
        if imagename in imagenames:
            image_id = imagenames[imagename]
        else:
            image_id = upload_image(baseurl, jwt, basepath + '/images',
                                    imagename, logprefix=' => ')
        annotationpath = imagepath\
            .replace('images', 'ground-truth')\
            .replace('.png', '.json')
        annotationname = imagename.replace('.png', '.json')
        if os.path.exists(annotationpath):
            print(' => annotation found for {}'.format(imagename))
            upload_annotation(baseurl, jwt, basepath + '/ground-truth',
                              annotationname, image_id, logprefix='     -> ')


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Usage: {} <base-url> <dataset-folder> <user-jwt>"
              .format(os.path.basename(sys.argv[0])))
        sys.exit(1)

    baseurl, datasetpath, jwt = sys.argv[1], sys.argv[2], sys.argv[3]

    if ('https' not in baseurl
            and 'localhost' not in baseurl
            and '127.0.0.1' not in baseurl):
        print('Not a local url and not https! Not sending this '
              'data on the wire in cleartext.')
        sys.exit(1)

    print("Uploading dataset files in '{}'".format(datasetpath))
    upload_dataset(baseurl, jwt, datasetpath)
